import sounddevice as sd
import numpy as np
import sys
from enum import IntEnum

class Notenhohe(IntEnum):
    C = 0
    D = 2
    E = 4
    F = 5
    G = 7
    A = 9
    H = 11

class Notenwert(IntEnum):
    Achtel = 0
    Viertel = 1
    Halbe = 2
    Ganze = 3

RatioPerDuration = {Notenwert.Achtel  : 1.0/8,
                    Notenwert.Viertel : 1.0/4,
                    Notenwert.Halbe   : 1.0/2,
                    Notenwert.Ganze   : 1.0/1}

class Note:
    def __init__(self, pitch, duration):
        self.pitch = pitch
        self.duration = duration

class TempoGenerator:
    def __init__(self, tempo):
        self.tempo = tempo

    def notenwertToSeconds(self, notenwert):
        return (60.0/self.tempo)*(RatioPerDuration[notenwert]*4)

tempoGen = TempoGenerator(100)

def setzeTempo(tempo):
    global  tempoGen
    tempoGen = TempoGenerator(tempo)

def pitchToMidi(note):
    return 60 + note

def midiToFreq(midi):
    return 2 ** ((midi - 69) / 12) * 440

def applyFades(samples):
    fadeLength = min(1000, len(samples)/2.0)
    samples[:fadeLength] = (np.arange(fadeLength)/fadeLength)*samples[:fadeLength]
    samples[-fadeLength:] = (np.arange(fadeLength, 0, -1)/fadeLength)*samples[-fadeLength:]

def playSound(freq, duration):
    fs = 44100.0
    amp = 0.2

    samples = amp*(np.sin(2*np.pi*np.arange(fs*duration) *freq/fs)).astype(np.float32)
    applyFades(samples)

    sd.play(np.transpose(samples), fs)
    sd.wait()

def playList(noteList):
    for note in noteList:
        playSound(midiToFreq(pitchToMidi(note.pitch)), tempoGen.notenwertToSeconds(note.duration))

def main(args):
    noten = [
    Note(Notenhohe.C, Notenwert.Viertel),
    Note(Notenhohe.D, Notenwert.Viertel),
    Note(Notenhohe.E, Notenwert.Viertel),
    Note(Notenhohe.F, Notenwert.Viertel),
    Note(Notenhohe.G, Notenwert.Halbe),
    Note(Notenhohe.G, Notenwert.Halbe),
    Note(Notenhohe.A, Notenwert.Viertel),
    Note(Notenhohe.A, Notenwert.Viertel),
    Note(Notenhohe.A, Notenwert.Viertel),
    Note(Notenhohe.A, Notenwert.Viertel),
    Note(Notenhohe.G, Notenwert.Ganze),
    Note(Notenhohe.F, Notenwert.Viertel),
    Note(Notenhohe.F, Notenwert.Viertel),
    Note(Notenhohe.F, Notenwert.Viertel),
    Note(Notenhohe.F, Notenwert.Viertel),
    Note(Notenhohe.E, Notenwert.Halbe),
    Note(Notenhohe.E, Notenwert.Halbe),
    Note(Notenhohe.D, Notenwert.Viertel),
    Note(Notenhohe.D, Notenwert.Viertel),
    Note(Notenhohe.D, Notenwert.Viertel),
    Note(Notenhohe.D, Notenwert.Viertel),
    Note(Notenhohe.C, Notenwert.Ganze),
        ]

    setzeTempo(160)
    playList(noten)


if __name__ == "__main__":
    main(sys.argv[1:])
    try:
        pass  # main(sys.argv[1:])
    except Exception as exc:
        print(exc)